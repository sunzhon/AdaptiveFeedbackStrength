#include "ModularNeural.h"

ModularNeural::ModularNeural( int nCPGs) {
	//1) initial
		n_CPGs = nCPGs;
		mLINs.resize(nCPGs);
		mCPGs.resize(nCPGs);
		mPCPGs.resize(nCPGs);
		mPSNs.resize(nCPGs);
		mVRNs.resize(nCPGs);
		mDelays.resize(nCPGs);
		mFMs.resize(nCPGs);
		mFFMs.resize(nCPGs);
		mPMNs.resize(nCPGs);
		mFFMs.resize(nCPGs);
		mDLs.resize(nCPGs);
		j1_output.resize(nCPGs);//motor out joint1 is upward and downward
		j2_output.resize(nCPGs);//motor out joint2 is forward and backward
		j3_output.resize(nCPGs);//motor out joint3  is extension and flexsion
		attiReflex.resize(nCPGs);//in order to attitude control
		grf.resize(nCPGs);
		jangle.resize(nCPGs);
	//2) new the spinal cord  neural network
		//********CPGs+PSNs+VRNs*******//
		for (int i = 0; i < nCPGs; i++) {
			//2.0) initial
			jangle.at(i).resize(3);//3 joints
			attiReflex.at(i).resize(3);//3 joints to response for the reflexs
			//2.1) input neurons
			mLINs.at(i) = new stcontroller::LIN();
			//2.2) CPGs
			mCPGs.at(i) = new CPG(0.32,0.24);
			//2.3) PCPGs
			//这里使用虚拟的post processing of CPG
			mPCPGs.at(i) = new PCPG();
			w(mPCPGs.at(i)->getNeuron(0), mCPGs.at(i)->getNeuron(0), 1);
			w(mPCPGs.at(i)->getNeuron(1), mCPGs.at(i)->getNeuron(1), 1);
			//2.4) PSNs
			mPSNs.at(i) = new PSN();
			//connection of input I1 with PSN
			w(mPSNs.at(i)->getNeuron(0), mLINs.at(i)->getNeuron(1), -1);
			w(mPSNs.at(i)->getNeuron(1), mLINs.at(i)->getNeuron(1), 1);

			//2/5) cpg to PSN
			w(mPSNs.at(i)->getNeuron(2), mPCPGs.at(i)->getNeuron(0), 0.5);
			w(mPSNs.at(i)->getNeuron(5), mPCPGs.at(i)->getNeuron(0), 0.5);

			w(mPSNs.at(i)->getNeuron(3), mPCPGs.at(i)->getNeuron(1), 0.5);
			w(mPSNs.at(i)->getNeuron(4), mPCPGs.at(i)->getNeuron(1), 0.5);

			//VRNs
			mVRNs.at(i) = new VRN();
			//cpg to VRN ,suntao add this
			w(mVRNs.at(i)->getNeuron(0), mPSNs.at(i)->getNeuron(11), 1.75);
			w(mVRNs.at(i)->getNeuron(1), mLINs.at(i)->getNeuron(2), 5);
			//PMN
			mPMNs.at(i)=new PMN(3);//each leg have three activity joint
			// suntao add this motor neuron connected to vrn and psn
			w(mPMNs.at(i)->getNeuron(0),mPSNs.at(i)->getNeuron(10),1.0);
			w(mPMNs.at(i)->getNeuron(1),mVRNs.at(i)->getNeuron(6),0.4*0.6);
			w(mPMNs.at(i)->getNeuron(2),mPSNs.at(i)->getNeuron(10),0.8*0.6);
			//suntao add this to connect I1 to Joint1 侧摆关节
			w(mPMNs.at(i)->getNeuron(0),mLINs.at(i)->getNeuron(0),10);//抑制侧摆关节至最大角度
			/*shoudong gengxin
			 *
			 * addSubnet(mLINs.at(i));
			addSubnet(mCPGs.at(i));
			addSubnet(mPCPGs.at(i));
			addSubnet(mPSNs.at(i));
			addSubnet(mVRNs.at(i));
			addSubnet(mLINs.at(i));
			addSubnet(mPMNs.at(i));*/
			//force foward model
			mFFMs.at(i)=new ForceForwardmodel();
			//double learn
			mDLs.at(i)=new DLearn();
			//forward model
			mFMs.at(i)=new Forwardmodel(true);
		}
	//3) new the vestibule neural network
		ATF =new stcontroller::AttitudeFeedback();

}

void ModularNeural::update(int ID) {
	//1) update input neuron
	mLINs.at(ID)->updateWeights();
	mLINs.at(ID)->updateActivities();
	mLINs.at(ID)->updateOutputs();

	//2) update CPGs
	mCPGs.at(ID)->setInput(grf.at(ID));//force feedback to this
	mCPGs.at(ID)->setGamma(0.1/*mDLs.at(ID)->getAdaptiveWeight()*/);//set sensory feedback strength to CPG
	mCPGs.at(ID)->updateWeights();//hide
	mCPGs.at(ID)->updateCPGActivities();// could not hide updateActivities();
	mCPGs.at(ID)->updateOutputs();
	//3) update PSCG
	mPCPGs.at(ID)->updateActivities();
	mPCPGs.at(ID)->updateOutputs();
	// 4) update PSN
	mPSNs.at(ID)->updateActivities();
	mPSNs.at(ID)->updateOutputs();
	// 5) update VRN
	mVRNs.at(ID)->updateActivities();
	mVRNs.at(ID)->updateOutputs();
	//5) update motor neuron
	mPMNs.at(ID)->setInput(0,0.0/*-1.1*attiReflex.at(ID).at(2)*/);//L2/L1
	mPMNs.at(ID)->setInput(1,-1.1*attiReflex.at(ID).at(2));//L2/L1
	mPMNs.at(ID)->setInput(2,attiReflex.at(ID).at(2));
	mPMNs.at(ID)->updateActivities();
	mPMNs.at(ID)->updateOutputs();	//6) update the CPG feedback
	//6) mFFMs force fwardmodel
	mFFMs.at(ID)->setInput(mPMNs.at(ID)->getOutput(2));
	mFFMs.at(ID)->step();
	//mFMs.at(ID)->step(mPMNs.at(ID)->getOutput(2),mPMNs.at(ID)->getOutput(2),mCPGs.at(ID)->getP());//return d
	//7) double learning rate lean
	mDLs.at(ID)->setInput(mFFMs.at(ID)->getOutput(),grf.at(ID));//input x,d or,x,y
	mDLs.at(ID)->step();//input x,d or,x,y
	//7) 更新PMN output 结果到J_output
	updateJointMotor(ID);
}


void ModularNeural::setInputNeuronInput(int ID, double l, double r, double f,
		double b) {
	setInput(mLINs.at(ID)->getNeuron(0), l);
	setInput(mLINs.at(ID)->getNeuron(1), r);
	setInput(mLINs.at(ID)->getNeuron(2), f);
	setInput(mLINs.at(ID)->getNeuron(3), b);
}

void ModularNeural::updateAttitudeControl(){
	ATF->setOriInput(ori_pitch,ori_roll);
	ATF->setGRFInput(grf.at(0),grf.at(1),grf.at(2),grf.at(3));
	ATF->update();
	for(unsigned int i=0;i<attiReflex.size();i++){//4 legs
		attiReflex.at(i).at(2)=attiReflex.at(i).at(2)+ATF->getOutput(i);
		attiReflex.at(i).at(2)= attiReflex.at(i).at(2) > 0.5 ? 0.5: (attiReflex.at(i).at(2)<-0.5 ? -0.5: attiReflex.at(i).at(2));
	}
}
double ModularNeural::getInputNeuronInput(int ID, unsigned int neuron_index){
	if(neuron_index>3)
		std::cerr<<"input neuron index out range "<<endl;
	return getInput(mLINs.at(ID)->getNeuron(neuron_index));
}
double ModularNeural::getInputNeuronOutput(int ID,int neuron_index) {
	if(neuron_index>3)
		std::cerr<<"input neuron index out range "<<endl;
	return getOutput(mLINs.at(ID)->getNeuron(neuron_index));
}

void ModularNeural::setGamma(int ID,double _g1){
	mCPGs.at(ID)->setGamma(_g1);
}
void ModularNeural::setCpgMi(int ID, double mi){
	mCPGs.at(ID)->setMi(mi);
}
void ModularNeural::setFootSensorForce(int ID,double _grf){
	// update mi input neuron
	grf.at(ID)=_grf;
}
void ModularNeural::setJointSensorAngle(int ID,double _j1,double _j2,double _j3){
	// update mi input neuron
	jangle.at(ID).at(0)=_j1;
	jangle.at(ID).at(1)=_j2;
	jangle.at(ID).at(2)=_j3;

}
double ModularNeural::getVrnOut(int ID) {
	return mVRNs.at(ID)->getOutput(6);
}

void ModularNeural::updateJointMotor(unsigned int ID){
	j1_output.at(ID)=mPMNs.at(ID)->getOutput(0);
	j2_output.at(ID)=mPMNs.at(ID)->getOutput(1);
	j3_output.at(ID)=mPMNs.at(ID)->getOutput(2);
}


double ModularNeural::getCpgOut0(int ID) {
	return mCPGs.at(ID)->getOutput(0);
}

double ModularNeural::getCpgOut1(int ID) {
	return mCPGs.at(ID)->getOutput(1);
}

double ModularNeural::getCpgOut2(int ID) {
	return mCPGs.at(ID)->getOutput(2);
}
double ModularNeural::getCpgOutP(int ID){
	return mCPGs.at(ID)->getP();
}
double ModularNeural::getCpgFrequency(int ID) {
	return mCPGs.at(ID)->getFrequency();
}
double ModularNeural::getPsnOutput(int ID, int neuron_index) {

	return mPSNs.at(ID)->getOutput(neuron_index);
}
double ModularNeural::getPcpgOutput(int ID, int neuron_index) {
	return mPCPGs.at(ID)->getOutput(neuron_index);
}

double ModularNeural::getPmnOut(int ID,int neuron_index){
	return mPMNs.at(ID)->getOutput(neuron_index);
}

/*

void ModularNeural::setFMb(int ID,double b){
	 mFMs.at(ID)->bias=b;
}
double ModularNeural::getFMOutput(int ID){
	return mFMs.at(ID)->outputfinal;
}
<<<<<<< HEAD

double ModularNeural::getFMOutputfinal(int ID){
	return mFMs.at(ID)->outputfinal;
}
double ModularNeural::getFMError(int ID){
	return mFMs.at(ID)->error;
}
double ModularNeural::getFMLearnError(int ID){
	return mFMs.at(ID)->learning_error;
}
=======
double ModularNeural::getFMInput(int ID){
	return mFMs.at(ID)->input;
}*/

/*


>>>>>>> b3
double ModularNeural::getFMLowpassError(int ID){
	return mFMs.at(ID)->lowpass_error;
}
void ModularNeural::setFMLearnrate(int ID,double learnrate){
		mFMs.at(ID)->learnrate=learnrate/10;
		mFMs.at(ID)->counter=0;
}
*/
double ModularNeural::getFMcounter(int ID)const{
	return mFMs.at(ID)->getCounter();
}
double ModularNeural::getFMw20(int ID)const{
	return mFMs.at(ID)->getW20();
}
double ModularNeural::getFMLearnError(int ID){
	return mFMs.at(ID)->getLearnerror();
}
double ModularNeural::getFMOutputfinal(int ID)const{
	return mFMs.at(ID)->getOutputfinal();
}

double ModularNeural::getFFMInput(int ID)const{
	return mFFMs.at(ID)->getInput();
}
double ModularNeural::getDLActualInput(int ID)const{
	return mDLs.at(ID)->getActualInput();
}
double ModularNeural::getDLExpectedInput(int ID)const{
	return mDLs.at(ID)->getExpectedInput();
}
double ModularNeural::getDLOutput(int ID)const{
	return mDLs.at(ID)->getAdaptiveWeight();
}
double ModularNeural::getFFMOutput(int ID)const{
	return mFFMs.at(ID)->getOutput();
}
double ModularNeural::getDLError(int ID)const{
	return mDLs.at(ID)->getError();
}
double ModularNeural::getDLBias(int ID ){
	return mDLs.at(ID)->getAdaptiveBias();
}
<<<<<<< HEAD
double ModularNeural::getDLMotorInput(int ID){
	return mDLs.at(ID)->getMotorInput();
=======

void ModularNeural::setAttituteInput(double _pitch,double _roll){
	ori_pitch=_pitch;
	ori_roll=_roll;
}

double ModularNeural::getAttiReflex(unsigned int ID,unsigned int joint)const{
	assert(ID <n_CPGs);
	return attiReflex.at(ID).at(joint);
>>>>>>> b3
}
