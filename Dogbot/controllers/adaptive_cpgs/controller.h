#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <selforg/abstractcontroller.h>
#include <selforg/controller_misc.h>
#include <selforg/configurable.h>
#include <selforg/types.h>

#include <assert.h>
#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include <selforg/matrix.h>
#include "utils/ann-framework/neuron.cpp"
#include "utils/ann-framework/synapse.cpp"
#include "utils/ann-framework/ann.cpp"
#include "utils/ann-library/vrn.cpp"
#include "ModularNeural.cpp"
#include <ode_robots/lildogsensormotordefinition.h>
#include "utils/lowPassfilter.cpp"
//#include <controllers/dogbot/adaptive_cpgs/lowPassfilter.cpp>






void getCommand(char key);

namespace stcontroller{
  struct modularNeuroControllerConf{
//1) add parameter
	  //1.1) input of whole net
    parameter stJ1SwitchFR;
    parameter stJ1SwitchRR;
    parameter stJ1SwitchFL;
    parameter stJ1SwitchRL;
    //1.2) input of PSN
   parameter stPsnSwitchFR;
   parameter stPsnSwitchFL;
   parameter stPsnSwitchRR;
   parameter stPsnSwitchRL;
   //1.3) input of VRN
   parameter stVrnSwitchFR;
   parameter stVrnSwitchFL;
   parameter stVrnSwitchRR;
   parameter stVrnSwitchRL;
   //1.4) MI of CPG
   parameter stMI;

//2) add Inspectvalue
   //2.1) plot of PCPG
   parameter stFR_PCPG_H0;
   parameter stFR_PCPG_H1;

   //2.2) plot of PSN
   parameter stFR_PSN_10;
   parameter stFR_PSN_11;

   //2.3) plot of VRN
   parameter stFR_VRN;

   //2.4) plot of VRN
   parameter stFR_PMN_0;
   parameter stFR_PMN_1;
   parameter stFR_PMN_2;


   //2.5) plot of FM
   parameter stFMOutput;
   parameter stFMOutputfinal;
<<<<<<< HEAD


   parameter stFMrw;
   parameter stFMb;

=======
   parameter stFMInput;
   parameter stFMw20;
   parameter stFMcounter;
>>>>>>> b3
   parameter stFMError;
   parameter stFMLearnError;
   parameter stFMLowpassError;
   parameter stFMLearnrate;

   //2.6) plot of FFM
   parameter stFFMOutput;
   parameter stFFMInput;

   //2.7) plot of DL
   parameter stDLOutput;
   parameter stDLError;
   parameter stDLBias;
<<<<<<< HEAD
   parameter stDLInput;




=======
   parameter stDLActualInput;
   parameter stDLExpectedInput;

   //2.8) plot of ATF
   parameter stAttiReflexFR;
   parameter stAttiReflexRR;
   parameter stAttiReflexFL;
   parameter stAttiReflexRL;

   //2.9) plot of filter of force
   parameter stGrfFR;
   parameter stGrfRR;
   parameter stGrfFL;
   parameter stGrfRL;

   //2.10) plot of Orientation of body
   parameter stOriPitch;
   parameter stOriRoll;
   //2.11) plot of gamma of CPG
   parameter stGammaFR;
   parameter stGammaFL;
   parameter stGammaRR;
   parameter stGammaRL;
//3) for debug mode ,directly to control every joint
   double stFRJ1;
   double stFRJ2;
   double stFRJ3;
   double stFLJ1;
   double stFLJ2;
   double stFLJ3;
   double stRLJ1;
   double stRLJ2;
   double stRLJ3;
   double stRRJ1;
   double stRRJ2;
   double stRRJ3;
 //4) initial function for parameter
   modularNeuroControllerConf(){
   	stPsnSwitchFR=0.0;
   	stPsnSwitchFL=0.0;
   	stPsnSwitchRR=0.0;
   	stPsnSwitchRL=0.0;

   	stVrnSwitchFR=0.4;
   	stVrnSwitchFL=0.4;
   	stVrnSwitchRR=0.4;
   	stVrnSwitchRL=0.4;

   	stJ1SwitchFR=1;
   	stJ1SwitchRR=1;
   	stJ1SwitchFL=1;
   	stJ1SwitchRL=1;

   	stMI=0.08;//0.075;
   }
>>>>>>> b3
  };


class modularNeuroController : public AbstractController {

  private:
    //utility function to draw outputs of the neurons
  modularNeuroControllerConf conf;
    void updateGui();
    bool debug_mode;//调试模式和工作模式



  public:
  //class constructor
    modularNeuroController( const modularNeuroControllerConf& c );

    modularNeuroController(int lilDogtype,bool mCPGs,bool mMuscleModelisEnabled);
    void initialize(int lilDogtype,bool mCPGs,bool mMuscleModelisEnabled);
    
  //class destructor
    virtual ~modularNeuroController(){}

    virtual void init(int sensornumber, int motornumber, RandGen* randGen = 0);

    double sigmoid(double num){
          return 1.0 / (1.0 + exp(-num));
      }
   
    /// returns the name of the object (with version number)
      virtual paramkey getName() const {
          return name;
      }
    /// returns the number of sensors the controller was initialised with or 0 if not initialised
      virtual int getSensorNumber() const {
          return numbersensors;
      }
    /// returns the mumber of motors the controller was initialised with or 0 if not initialised
      virtual int getMotorNumber() const {
          return numbermotors;
      }
    //perform one step
    virtual void step(const sensor*, int number_sensors, motor*, int number_motors);

    /// performs one step without learning. Calulates motor commands from sensor inputs.
      virtual void stepNoLearning(const sensor*, int number_sensors, motor*, int number_motors) {
      // empty
      }

    /***** STOREABLE ****/
    /** stores the controller values to a given file. */
    virtual bool store(FILE* f) const;
    /** loads the controller values from a given file. */
    virtual bool restore(FILE* f);
    virtual double getGaitDiagram(double GRF);



  private:
  ModularNeural *mnc;
  bool mul_cpgs;
//gui shuchu ,ca test cpg pcpg or motor signal,this depend on predefine
  double R0_H0;
  double R0_H1;
  double R0_P;

  //gui L0
  double L0_H0;
  double L0_H1;
  double L0_P;

  //gui L1
  double L1_H0;
  double L1_H1;
  double L1_P;

  //gui R1
  double R1_H0;
  double R1_H1;
  double R1_P;


  double timer;

  parameter omega0,omega1,omega2,omega3;
    //not relevant in the current lilDog
    int lilDogType;
    bool sensoryFeed;
    bool muscleModel;



  protected:
    
    unsigned short numbersensors, numbermotors;
    paramkey name;
    int t;

  public:
    // inputs and outputs of serves of every joints
    std::vector<sensor> x;
    std::vector<sensor> y;

    //low pass filter
    std::vector<lowPass_filter *> filterGRF;
    std::vector<lowPass_filter *> filterOri;
    std::vector<lowPass_filter *> filterJangle;

    // post processing value of sensor value
    std::vector<double > GRForce;
    std::vector<double > Orientation;
    std::vector<double > JointAngle;


    /////////////////////////////////////////////

};

}
#endif
