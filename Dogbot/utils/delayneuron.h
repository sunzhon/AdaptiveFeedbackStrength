/*
 * delayneuron.h
 *
 *  Created on: 2018-3-3
 *      Author: suntao
 */

#ifndef DELAYNEURON_H_
#define DELAYNEURON_H_

#include <utils/ann-framework/neuron.h>
#include "delayline.h"

namespace stcontroller {

class DelayNeuron: public Neuron {
public:
	DelayNeuron(int size);
	virtual ~DelayNeuron();
private:
	Delayline *Delay;
};

} /* namespace stcontroller */

#endif /* DELAYNEURON_H_ */
