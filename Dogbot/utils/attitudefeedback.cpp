/*
 * attitutefeedback.cpp
 *
 *  Created on: Feb 4, 2018
 *      Author: suntao
 */

#include <utils/attitudefeedback.h>

namespace stcontroller {

AttitudeFeedback::AttitudeFeedback() {
	// TODO Auto-generated constructor stub
	pitch_Kp=.25;
	pitch_Ki=0.4;
	pitch_Kd=0.0;

	roll_Kp=0.2;
	roll_Ki=0.35;
	roll_Kd=0.0;

	pitch=0.0;
	pitch_old=0.0;
	pitch_2old=0.0;
	 roll=.0;
	 roll_old=.0;
	 roll_2old=.0;
	 output.resize(LEG_NUM);
	 grf.resize(LEG_NUM);
}

AttitudeFeedback::~AttitudeFeedback() {
	// TODO Auto-generated destructor stub
}



void AttitudeFeedback::step(double _pitch,double _roll){

	;
}
void AttitudeFeedback::setOriInput(double _pitch,double _roll){
	pitch=_pitch;
	roll=_roll;
 	//std::cout<<"pitch:"<<pitch<<"roll"<<roll<<std::endl;
}
void AttitudeFeedback::setGRFInput(double _grfFR,double _grfRR,double _grfFL,double _grfRL){
	grf.at(0)=_grfFR;
	grf.at(1)=_grfRR;
	grf.at(2)=_grfFL;
	grf.at(3)=_grfRL;
 	//std::cout<<"FR:"<<grf.at(0)<<" RR:"<<grf.at(1)<<" FL:"<<grf.at(2)<<" RL:"<<grf.at(3)<<std::endl;

}
void AttitudeFeedback::update(){
		//1) 姿态
		//预处理,限幅
		pitch= pitch > 0.03 ? pitch: (pitch < -0.03 ? pitch : 0);
		roll= roll > 0.03 ? roll: (roll > -0.03 ? 0: roll);
		//增量 数字PID
		pitch_output=pitch_Kp*(pitch-pitch_old)+pitch_Ki*pitch+pitch_Kd*(pitch-2*pitch_old+pitch_2old);
		roll_output=roll_Kp*(roll-roll_old)+roll_Ki*roll+roll_Kd*(roll-2*roll_old+roll_2old);
		 output.at(0)=pitch_output+roll_output;
		 output.at(1)=-1.0*pitch_output+roll_output;
		 output.at(2)=pitch_output-roll_output;
		 output.at(3)=-1.0*pitch_output-roll_output;
		 // 只有支撑腿才用于调节姿态
		 for(unsigned int i=0;i<grf.size();i++){
			 if(grf.at(i) <  0.1)//摆动相
				 output.at(i)=0.0;
		 }
		 for(unsigned int i=0;i<output.size();i++){
			 //缩腿时pmn_bias 增大，所以
			 output.at(i)=-1.0*output.at(i);
			 //限幅
			 output.at(i)= output.at(i) > 0.4 ? 0.4: (output.at(i)<-0.4 ? -0.4: output.at(i));
		 }
		 //2） 足底和重心投影点的相对位置


		 // 变量存储
		 pitch_2old=pitch_old;
		 pitch_old=pitch;
		 roll_2old=roll_old;
		 roll_old=roll;
}
double AttitudeFeedback::getOutput(unsigned int ID)const {
	assert(ID < LEG_NUM);
	return output.at(ID);
}


}
/* namespace stcontroller */
