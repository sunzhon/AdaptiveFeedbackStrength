/*
 * dLearn.cpp

 *
 *  Created on: Dec 8, 2017
 *      Author: suntao
 */

#include "dlearn.h"
/*
 * This class implement adaptive sensory feedback strength to CPG and adaptive bias of motor neurons by
 * the difference  between the contact sensory and expected contact value .
 *
 *
 * */
ForceForwardmodel::ForceForwardmodel(){
		gama=0.4;
		output_old=0.0;
		input_old=0.0;
		input=0.0;
		output=0.0;
	}
ForceForwardmodel::~ForceForwardmodel(){

};
void ForceForwardmodel::setInput(double _input){
		input=_input;
	}
	void ForceForwardmodel::step(){
		double F;
		if(input>input_old)
				G=0.0;
			else
				G=1.0;
		F=gama*G+(1-gama)*output_old;
		output=0.98*F;//-0.8*F+0.8;
		output_old=output;
		input_old=input;
	}
	double ForceForwardmodel::getOutput()const{
		return output;
	}
	double ForceForwardmodel::getInput()const{
		return input;
	}


DLearn::DLearn() {

	// TODO Auto-generated constructor stub
	//weight
	As=0.993;
	Af=0.57;
	Bs=0.0002;
	Bf=0.003;
	Kf_old=0.0;
	Ks_old=0.0;
	//bias
	As_e=0.980;
	Af_e=0.570;
	Bs_e=0.02;
	Bf_e=0.03;
<<<<<<< HEAD
	//forward model
	gama=0.5;
=======
	Kf_old_e=0.0;
	Ks_old_e=0.0;

	actual_force_input=0.0;
	expected_force_input=0.0;

>>>>>>> b3


}

DLearn::~DLearn() {
	// TODO Auto-generated destructor stub
}
<<<<<<< HEAD
double DLearn::step(double _motor_input,double _sensor_input,bool fmtype/*decide use which forward model,if =true,please  have to activate mFDs*/){
	if(fmtype==false){	//sensor_input=d,motor_input=x,F=y
		motor_input=_motor_input;
		sensor_input=_sensor_input;
		motor=-1*motor_input;
		if(motor>motor_old)
				G=1;
			else
				G=0;
		motor_old=motor;
		F=gama*G+(1-gama)*F_old;
		F2=1/(1+exp(-F*10));
		//F2=F;
		error=sensor_input-F;

	}else{//use  another forward model	,//sensor_input=d,motor_input=y,
		error=sensor_input-motor_input;
	}
	// learn to produce adaptive weight for sensory feedback strength
	F_old=F;
	Kf_old=Kf;
	Ks_old=Ks;
=======
void DLearn::step(){
	//0) calculate the error
		error=actual_force_input-expected_force_input;
	//1) learn to produce adaptive weight for sensory feedback strength
>>>>>>> b3
	Kf=Af*Kf_old+Bf*error;
	Ks=As*Ks_old+Bs*error;
	weight=10*(Kf+Ks)+0.05;//4.5*(Kf+Ks)+0.045;
	weight=weight > 0 ? weight:0.0;
	Kf_old=Kf;
	Ks_old=Ks;
	//2) learn to produce adaptive bias for PMNs
	Kf_e=Af_e*Kf_old_e+Bf_e*error;
    Ks_e=As_e*Ks_old_e+Bs_e*error;
    bias=(Kf_e+Ks_e-0.25)*0.5;
	Kf_old_e=Kf_e;
	Ks_old_e=Ks_e;

}
void DLearn::setInput(double expected_force,double actual_force){
	expected_force_input=expected_force;
	actual_force_input=actual_force;
}

double DLearn::getAdaptiveWeight(){
	return weight;
}
double DLearn::getExpectedInput(){
	return expected_force_input;
}
double DLearn::getActualInput(){
	return actual_force_input;
}
double DLearn::getAdaptiveBias(){
	return bias;
}

double DLearn::getError(){
	return error;
}
<<<<<<< HEAD

void DLearn::setGama(double _g){
	gama=_g;
}
double DLearn::getMotorInput(void){
	return motor_input;
}
double DLearn::getSensorInput(void){
	return sensor_input;
}
=======
>>>>>>> b3
