#ifndef PMN_H_
#define PMN_H_

#include "../ann-framework/ann.h"
//#include <ode_robots/amosiisensormotordefinition.h>


class PMN : public ANN {
public:
    PMN(unsigned int _JointNum);
private:
    unsigned int JointNum;
};


#endif /* PMN_H_ */
