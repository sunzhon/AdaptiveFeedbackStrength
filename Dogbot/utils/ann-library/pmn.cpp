#include "pmn.h"


PMN::PMN(unsigned int _JointNum)
{
	setDefaultTransferFunction(identityFunction());
	setAllTransferFunctions(identityFunction());
    setNeuronNumber(_JointNum);
    b(0,0.0);
    b(1,0.0);
    b(2,0.0);
    JointNum=_JointNum;
}
