#include "cpg.h"

using namespace std;

CPG::CPG(double _gamma1,double _gamma2){ //initial the gama refer to Multiple decoupled cpg...paper

	//three neuron network with 2 plastic synapses
	setNeuronNumber(2);// CPG is tanhFunction
	setDefaultTransferFunction(identityFunction());//P neuron is identityFunction
	P=addNeuron();
	Delay = new Delayline(10);
	gamma.resize(2);
	gamma.at(0)=_gamma1;
	gamma.at(1)=_gamma2;
	a_t.resize(2);//all of those is vector type
	a_t1.resize(2);
	feed.resize(2);

	MI=0.1;//modification
	w(0,0,1.4);
	w(0,1,0.18+MI);
	w(1,0,-0.18-MI);
	w(1,1,1.4);
	b(0,0.01);
	b(1,0.01);

	setOutput(0,0.02);
	setOutput(1,0.02);

}

void CPG::updateWeights(){

/* equal to w(0,0,1.4);but this can not to create synapse ,if it not exist
 * Synapse * synapse = neurons[0]->getSynapseFrom(neurons[0]);
    synapse->setWeight(1.4);*/

	w(0,0,1.4);
	w(0,1,0.18+MI);
	w(1,0,-0.18-MI);
	w(1,1,1.4);
}


double CPG::getOut0(){
	return getOutput(0);
}
double CPG::getOut1(){
	return getOutput(1);
}
double CPG::getOut2(){
	return getOutput(2);
}
double CPG::getInput(){
	return input;
}
double CPG::getP(){
	return getOutput(P);
}
void CPG::setP(double p){
	P->setOutput(p);
}
double CPG::getGamma(unsigned int id){
	if(id>=2)
		std::cout<<"id is out range"<<endl;
	return gamma.at(id);
}
void CPG::setGamma(double g1){
	gamma.at(0)=g1;
	gamma.at(1)=g1+0.005;
}
void CPG::setMi(double mi){
	MI=mi;
}
void CPG::setInput(double p){

	input=p;
	feed.at(0)=input;
	feed.at(1)=input;
}
void CPG::updateCPGActivities(){
		updateActivities();
		a_t.at(0) = getActivity(getNeuron(0));
		a_t.at(1) = getActivity(getNeuron(1));

		a_t1.at(0) = a_t.at(0) - getGamma(0) * feed.at(0)* cos(a_t.at(0));//+beta*mCPGs.at(previousID)->getOut0();
		a_t1.at(1) = a_t.at(1) - getGamma(1) *feed.at(1) * sin(a_t.at(1));
		setActivity(0, a_t1.at(0));
		setActivity(1, a_t1.at(1));
		// add a delayline to P neuron
		Delay->Write(getOutput(0));
		Delay->Step();
		setActivity(2,Delay->Read(5));

}
double CPG::getFrequency(){

/*	if(ALPHA != 1.01){
		cout << "Chose alpha = 1 + epsilon,to have a proportinal relationship between phi and frequency"<< endl;
		return -1;
	}
	return phi/(2*M_PI);*/
	return 4*MI+0.1;
}


