/*
 * attitutefeedback.h
 *
 *  Created on: Feb 4, 2018
 *      Author: suntao
 */

#ifndef ATTITUDEFEEDBACK_H_
#define ATTITUDEFEEDBACK_H_

namespace stcontroller {

class AttitudeFeedback {
public:
	enum Leg{
		FR=0,
		RR=1,
		FL=2,
		RL=3,
		LEG_NUM=4
	};
	AttitudeFeedback();
	virtual ~AttitudeFeedback();
	void step(double _pitch,double _roll);
	void setOriInput(double _pitch,double _roll);
	void update();
	void setGRFInput(double _grfFR,double _grfRR,double _grfFL,double _grfRL);
	double getOutput(unsigned int ID) const;
private:
	double pitch,pitch_old,pitch_2old;//roll
	double roll,roll_old,roll_2old;//roll
	double pitch_output,roll_output;
	std::vector<double > grf;
	std::vector<double> output;//as a bias of pmn

	//pid parameters
	double pitch_Kp,pitch_Ki,pitch_Kd;
	double roll_Kp,roll_Ki,roll_Kd;
};

} /* namespace stcontroller */

#endif /* ATTITUTEFEEDBACK_H_ */
