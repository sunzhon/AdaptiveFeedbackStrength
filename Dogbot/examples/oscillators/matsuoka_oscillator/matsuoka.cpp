/*
 * matsuoka.cpp
 *
 *  Created on: Feb 25, 2018
 *      Author: suntao
 */

//#include <examples/oscillators/matsuoka_oscillator/matsuoka.h>
#include "matsuoka.h"
namespace stcontroller {

MATSUOKA::MATSUOKA(double TAU) {
	// TODO Auto-generated constructor stub
	// the following parameters were set for reference <Kimura.Adaptive Dynamic walking of Quadruped Robot On Irregular Terrain
	//  based on Biological Concepts>
	double u0=2.36;//1.0;
	gamma.resize(2);
	a_t.resize(2);
	a_t1.resize(2);
	 wfe=-2.0;
	setNeuronNumber(2);
	setDefaultTransferFunction(identityFunction());// addNeuron will use the default Transfer function
	setAllTransferFunctions(identityFunction());

	P=addNeuron();
	w(0,1,wfe);
	w(1,0,wfe);
	b(0,u0);
	b(1,u0);

	this->TAU=TAU;

	v1=0.0;
	u1=0.0;
	y1=0.0;
	v2=0.0;
	u2=0.0;
	y2=0.0;

	v1_dot=0.0;
	u1_dot=0.0;

	v2_dot=0.0;
	u2_dot=0.0;

	Tau_u=0.224;
	Tau_v=0.280;//0.6;
	beta=2.5;//3.0;


}
void MATSUOKA::setCPGInput(double _input){
	input=_input;
	feed1=input;
	feed2=-1*input;
	setInput(0,feed1);
	setInput(1,feed2);
}

MATSUOKA::~MATSUOKA() {
	// TODO Auto-generated destructor stub
}

void MATSUOKA::updateCPGActivities(){

// Neuron1
	updateActivities();
	a_t.at(0)=getActivity(0);
	a_t.at(1)=getActivity(1);

    u1_dot = (-u1 - beta * v1 +a_t.at(0)) / Tau_u;
    v1_dot = (-v1 + y1) / Tau_v;
    u1 += TAU * u1_dot;
    v1 += TAU * v1_dot;
    y1= u1 >0.0 ? u1 :0.0;
    a_t1.at(0)=y1;


// Neuron2
    u2_dot = (-u2 - beta * v2 +a_t.at(1)) / Tau_u;
    v2_dot = (-v2 + y2) / Tau_v;
    u2 += TAU * u2_dot;
    v2 += TAU * v2_dot;
    y2= u2 >0.0 ? u2 :0.0;
    a_t1.at(1)=y2;



	setActivity(0, a_t1.at(0));
	setActivity(1, a_t1.at(1));
    MatsuokaOutput=y1-y2;
    P->setActivity(MatsuokaOutput);

/*
    std::cout<<"input:"<<input<<std::endl;
    std::cout<<"bias1:"<<b(0)<<std::endl;
    std::cout<<"bias2:"<<b(1)<<std::endl;

    std::cout<<"a_t0:"<<a_t.at(0)<<std::endl;
    std::cout<<"a_t1:"<<a_t.at(1)<<std::endl;
    std::cout<<"u1:"<<u1<<std::endl;
    std::cout<<"u1:"<<u1<<std::endl;
    std::cout<<"v1:"<<v1<<std::endl;
    std::cout<<"v1:"<<v1<<std::endl;
    std::cout<<"y1:"<<y1<<std::endl;
    std::cout<<"y1:"<<y1<<std::endl;
    std::cout<<"getActivity0:"<<getActivity(0)<<std::endl;
    std::cout<<"getActivity1:"<<getActivity(1)<<std::endl;
    std::cout<<"MatsuokaOutput:"<<MatsuokaOutput<<std::endl;
*/



}

double MATSUOKA::getCPGOutput(){
	return MatsuokaOutput;
}

Neuron * MATSUOKA::getPNeuron(){
	return P;
}

double MATSUOKA::getOut0(){
	return getOutput(0);
}
double MATSUOKA::getOut1(){
	return getOutput(1);
}
double MATSUOKA::getOut2(){
	return getOutput(2);
}
double MATSUOKA::getInput(){
	return input;
}
double MATSUOKA::getP(){
	return getOutput(P);
}
void MATSUOKA::setP(double p){
	P->setOutput(p);
}
double MATSUOKA::getGamma(unsigned int id){
	if(id>=2)
		std::cout<<"id is out range"<<endl;
	return gamma.at(id);
}
void MATSUOKA::setGamma(double g1){
	gamma.at(0)=g1;
	gamma.at(1)=g1+0.005;
}
void MATSUOKA::setMi(double mi){
	wfe=mi;
}

double MATSUOKA::getFrequency(){

/*	if(ALPHA != 1.01){
		cout << "Chose alpha = 1 + epsilon,to have a proportinal relationship between phi and frequency"<< endl;
		return -1;
	}
	return phi/(2*M_PI);*/
	return 0.0;//4*MI+0.1;
}




} /* namespace stcontroller */
