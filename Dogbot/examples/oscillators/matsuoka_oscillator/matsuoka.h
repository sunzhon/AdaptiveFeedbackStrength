/*
 * matsuoka.h
 *
 *  Created on: Feb 25, 2018
 *      Author: suntao
 */

#ifndef MATSUOKA_H_
#define MATSUOKA_H_
#include <vector>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "utils/ann-framework/ann.h"
#include "utils/ann-framework/neuron.h"




namespace stcontroller {

class MATSUOKA : public ANN {

public:
	MATSUOKA(double TAU);
	virtual ~MATSUOKA();
	void updateCPGActivities();
	void setCPGInput(double _input);
	double getInput();
	double getCPGOutput();
	Neuron * getPNeuron();
	double getP();
	void setP(double p);

//get Gamma
	double getGamma(unsigned int id);
	double getFrequency();

//set Beta
	void setBeta(double b);
//set Gamma
	void setGamma(double g1);
	double getOut0();
//get output neuron 1
	double getOut1();
//get output neuron 2
	double getOut2();
	//set weight
	void setMi(double mi);
private:
	Neuron * P;
	double v1,u1,v2,u2,y1,y2;
	double v1_dot,u1_dot;
	double v2_dot,u2_dot;
	double Tau_u,Tau_v;
	double beta;
	double input;
	double feed1,feed2;
	double TAU;
	double MatsuokaOutput;

	double wfe;//MI;
	std::vector<double> gamma;
	std::vector<double> a_t;
	std::vector<double> a_t1;

};

} /* namespace stcontroller */

#endif /* MATSUOKA_H_ */

