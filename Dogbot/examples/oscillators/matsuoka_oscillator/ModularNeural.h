#ifndef MODULARNEURAL_H_
#define MODULARNEURAL_H_
#include <selforg/controller_misc.h>
#include <selforg/configurable.h>
#include <selforg/types.h>

#include <assert.h>
#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iostream>

#include <selforg/matrix.h>
#include "matsuoka.cpp"
#include "utils/legdelay.cpp"
#include "utils/ann-library/pcpg.cpp"
#include "utils/ann-framework/neuron.h"
#include "utils/ann-framework/synapse.h"
#include "utils/ann-framework/ann.h"
#include "utils/ann-library/vrn.h"
#include "utils/ann-library/psn.cpp"
#include "utils/ann-library/lin.cpp"
#include "utils/forwardmodel.cpp"
#include "utils/ann-library/pmn.cpp"
#include "utils/dlearn.cpp"
#include "utils/attitudefeedback.cpp"
//#include "utils/bpnet.h"


using namespace stcontroller;
//前置声明，在该类的定义之前，声明该类，告诉后面有这样一个类，在该类实现之前只能定义该类的指针，不能定义对象，只有
//在该类定义之后，才能定义该类的对象.  告诉编译器CPG,..是类类型。
class PCPG;
class VRN;
class PSN;
class LegDelay;
class Dlearn;
class AttitudeFeedback;
class BpNet;
typedef MATSUOKA CPG;
class ModularNeural : public ANN{
			public:
					ModularNeural(int nCPGs);
					~ModularNeural(){}

			void update(int ID);
			void updateJointMotor(unsigned int ID);
			void setInputNeuronInput(int ID,double l, double r, double f,double b); //left,right,forward,backward
			void setCpgMi(int ID,double mi);
			void setGamma(int ID,double _g1);
			void setGRFInput(int ID,double _grf);
			void setJAngleInput(int ID,int joint,double angle);

			void updateMotorlimit(int ID);
			void updateAttitudeControl();

			double getCpgOut0(int ID);
			double getCpgOut1(int ID);
			double getCpgOut2(int ID);
			double getCpgFrequency(int ID);
			double getPsnOutput(int ID,int neuron_index);
			double getPcpgOutput(int ID,int neuron_index);
			double getPmnOut(int ID,int neuron_index);
			double getVrnOut(int ID);
			double getInputNeuronOutput(int ID,int neuron_index);
			double getInputNeuronInput(int ID,unsigned int neuron_index);

			double getFMOutput(int ID);
			double getFMInput(int ID);
			double getFMOutputfinal(int ID)const;
			 void  setFMb(int ID,double b);
			double getFMw20(int ID)const;
			double getFMcounter(int ID)const;
			double getFMLearnError(int ID);
			double getFMLowpassError(int ID);
			void setFMLearnrate(int ID,double learnrate);//learnrate/10

			double getDLMInput(int ID)const;
			double getDLSInput(int ID)const;
			double getDLOutput(int ID)const;
			double getDLF(int ID)const;
			void setDLGama(int ID ,double _g);
			double getDLError(int ID)const;
			double getDLBias(int ID );

			double getAttitudePmnBias(unsigned int ID)const;// get attitute feedback output accumulation
			void setAttituteInput(double _pitch,double _roll);
			void setDLScale(int ID,double scale);//设置参数的数量级Af×scale
			private:
			stcontroller::LIN *lin;
				PCPG *pcpg;
				CPG *cpg;
				VRN *vrn_left;
				VRN *vrn_right;
				PSN *psn;
				std::vector<stcontroller::LIN*> mLINs;//控制机器人运动方向的输入神经元组,input neurons
				std::vector<CPG*> mCPGs;
				std::vector<PCPG*> mPCPGs;
				std::vector<PSN*> mPSNs;
				std::vector<VRN*> mVRNs;//sun tao  add this
				std::vector<LegDelay*> mDelays;
				std::vector<Forwardmodel *>mFMs;//forward model
				std::vector<DLearn *>mDLs;//双速率学习，double learn
				std::vector<PMN *> mPMNs;//运动神经元
				stcontroller::AttitudeFeedback *ATF;//姿态控制


				//double ** delta;
				//double ** k;
				//double cpgMI;
				double n_CPGs;
				int tau,tau_l;
				double osc_couple0,osc_couple1;
				int previousID;
				double beta;
				std::vector<double> attiPmnBias;//4legs
				double ori_pitch,ori_roll;//机身姿态
				std::vector<double > grf;//足底力
				std::vector<double> jangle;// joint angle
			public:
				vector<double> j1_output; 				    	//motor neural outputs
				vector<double> j2_output; 				    	//motor neural outputs
				vector<double> j3_output; 				    	//motor neural outputs

};
#endif
