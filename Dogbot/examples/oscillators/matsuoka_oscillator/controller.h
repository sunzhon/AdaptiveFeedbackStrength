#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include <selforg/abstractcontroller.h>
#include <selforg/controller_misc.h>
#include <selforg/configurable.h>
#include <selforg/types.h>

#include <assert.h>
#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include <selforg/matrix.h>
#include "utils/ann-framework/neuron.cpp"
#include "utils/ann-framework/synapse.cpp"
#include "utils/ann-framework/ann.cpp"
#include "utils/ann-library/vrn.cpp"
#include "ModularNeural.cpp"
#include <ode_robots/lildogsensormotordefinition.h>
#include "utils/lowPassfilter.cpp"
//#include <controllers/dogbot/adaptive_cpgs/lowPassfilter.cpp>



typedef double parameter;


void getCommand(char key);

namespace stcontroller{
  struct modularNeuroControllerConf{
//1) add parameter
    parameter stJ1SwitchFR;
    parameter stJ1SwitchRR;
    parameter stJ1SwitchFL;
    parameter stJ1SwitchRL;

   parameter stPsnSwitchFR;
   parameter stPsnSwitchFL;
   parameter stPsnSwitchRR;
   parameter stPsnSwitchRL;

   parameter stVrnSwitchFR;
   parameter stVrnSwitchFL;
   parameter stVrnSwitchRR;
   parameter stVrnSwitchRL;

   parameter stMI;
//2) add Inspectvalue
   parameter stFR_PCPG_H0;
   parameter stFR_PCPG_H1;

   parameter stFR_PSN_10;
   parameter stFR_PSN_11;

   parameter stFR_VRN;

   parameter stFR_PMN_1;
   parameter stFR_PMN_2;

   parameter stFMOutput;
   parameter stFMOutputfinal;
   parameter stFMInput;

   parameter stFMw20;
   parameter stFMcounter;

   parameter stFMError;
   parameter stFMLearnError;
   parameter stFMLowpassError;

   parameter stFMLearnrate;

   parameter stDLOutput;
   parameter stDLF;
   parameter stDLError;
   parameter stDLBias;
   parameter stDLSInput;
   parameter stDLMInput;

   parameter stGammaFR;
   parameter stGammaFL;
   parameter stGammaRR;
   parameter stGammaRL;

   parameter stGaitDiagramFR;
   parameter stGaitDiagramFL;
   parameter stGaitDiagramRR;
   parameter stGaitDiagramRL;

   parameter stAttiPmnBiasFR;
   parameter stAttiPmnBiasRR;
   parameter stAttiPmnBiasFL;
   parameter stAttiPmnBiasRL;

   parameter stGrfFR;
   parameter stGrfRR;
   parameter stGrfFL;
   parameter stGrfRL;

   parameter stOriPitch;
   parameter stOriRoll;
//3) for debug mode
   double stFRJ1;
   double stFRJ2;
   double stFRJ3;
   double stFLJ1;
   double stFLJ2;
   double stFLJ3;
   double stRLJ1;
   double stRLJ2;
   double stRLJ3;
   double stRRJ1;
   double stRRJ2;
   double stRRJ3;
 //4) initial function for parameter
   modularNeuroControllerConf(){
   	stPsnSwitchFR=0.0;
   	stPsnSwitchFL=0.0;
   	stPsnSwitchRR=0.0;
   	stPsnSwitchRL=0.0;

   	stVrnSwitchFR=0.4;
   	stVrnSwitchFL=0.4;
   	stVrnSwitchRR=0.4;
   	stVrnSwitchRL=0.4;

   	stJ1SwitchFR=1;
   	stJ1SwitchRR=1;
   	stJ1SwitchFL=1;
   	stJ1SwitchRL=1;

   	stMI=-2.0;//0.075;
   }
  };


class modularNeuroController : public AbstractController {

  private:
    //utility function to draw outputs of the neurons
  modularNeuroControllerConf conf;
    void updateGui();
    bool debug_mode;//调试模式和工作模式



  public:
  //class constructor
    modularNeuroController( const modularNeuroControllerConf& c );

    modularNeuroController(int lilDogtype,bool mCPGs,bool mMuscleModelisEnabled);
    void initialize(int lilDogtype,bool mCPGs,bool mMuscleModelisEnabled);
    
  //class destructor
    virtual ~modularNeuroController(){}

    virtual void init(int sensornumber, int motornumber, RandGen* randGen = 0);

    double sigmoid(double num){
          return 1.0 / (1.0 + exp(-num));
      }
   
    /// returns the name of the object (with version number)
      virtual paramkey getName() const {
          return name;
      }
    /// returns the number of sensors the controller was initialised with or 0 if not initialised
      virtual int getSensorNumber() const {
          return numbersensors;
      }
    /// returns the mumber of motors the controller was initialised with or 0 if not initialised
      virtual int getMotorNumber() const {
          return numbermotors;
      }
    //perform one step
    virtual void step(const sensor*, int number_sensors, motor*, int number_motors);

    /// performs one step without learning. Calulates motor commands from sensor inputs.
      virtual void stepNoLearning(const sensor*, int number_sensors, motor*, int number_motors) {
      // empty
      }

    /***** STOREABLE ****/
    /** stores the controller values to a given file. */
    virtual bool store(FILE* f) const;
    /** loads the controller values from a given file. */
    virtual bool restore(FILE* f);
    virtual double getGaitDiagram(double GRF);


  private:
  ModularNeural *mnc;
  bool mul_cpgs;
//gui shuchu ,ca test cpg pcpg or motor signal,this depend on predefine
  double R0_H0;
  double R0_H1;
  double R0_Pertubation;
  //gui L0
  double L0_H0;
  double L0_H1;
  double L0_Pertubation;

  //gui L1
  double L1_H0;
  double L1_H1;
  double L1_Pertubation;

  //gui R1
  double R1_H0;
  double R1_H1;
  double R1_Pertubation;


  double timer;

  parameter omega0,omega1,omega2,omega3;
    //not relevant in the current lilDog
    int lilDogType;
    bool sensoryFeed;
    bool muscleModel;



  protected:
    
    unsigned short numbersensors, numbermotors;
    paramkey name;
    int t;

  public:
    
    std::vector<sensor> x;
    std::vector<sensor> y;
    std::vector< std::vector<double> > y_MCPGs;
    std::vector<double > GRForce;
    std::vector<double > Orientation;
    std::vector<lowPass_filter *> filterGRF;
    std::vector<lowPass_filter *> filterOri;
    //low pass filtering

    /////////////////////////////////////////////

};

}
#endif
