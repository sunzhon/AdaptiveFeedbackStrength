#include <math.h>
#include "controller.h"
#define LEG_NUM 4
using namespace matrix;
using namespace std;
using namespace lpzrobots;
namespace stcontroller {
modularNeuroController::modularNeuroController(
		const modularNeuroControllerConf& c) :
		AbstractController("adaptivelocomotioncontroller",
				"$Id: controller.cpp,v 0.1-st $"), conf(c) {
	//1) initial
	GRForce.resize(LEG_NUM);
	filterGRF.resize(LEG_NUM);
	filterOri.resize(3); // 3 dimensions
	Orientation.resize(3); //3 dimensions
	initialize(2, true, false);
	debug_mode = false;
	//low pass filter to clean the feedback signals of the different legs,cut off frequency has been set to 0.3,
	//setting it to a frequency bigger than 0.4 results in too much sensitive adaptive oscillator's response
	for (unsigned int i = 0; i < filterGRF.size(); i++)
		filterGRF.at(i) = new lowPass_filter(0.7);
	for (unsigned int i = 0; i < filterOri.size(); i++)
		filterOri.at(i) = new lowPass_filter(0.7);

	R0_H0 = mnc->getCpgOut0(0);
	R0_H1 = mnc->getCpgOut1(0);
	R0_Pertubation = mnc->getCpgOut2(0);

	R1_H0 = mnc->getCpgOut0(1);
	R1_H1 = mnc->getCpgOut1(1);
	R1_Pertubation = mnc->getCpgOut2(1);

	L0_H0 = mnc->getCpgOut0(2);
	L0_H1 = mnc->getCpgOut1(2);
	L0_Pertubation = mnc->getCpgOut2(2);

	L1_H0 = mnc->getCpgOut0(3);
	L1_H1 = mnc->getCpgOut1(3);
	L1_Pertubation = mnc->getCpgOut2(3);

	omega0 = mnc->getCpgFrequency(0);
	omega1 = mnc->getCpgFrequency(1);
	omega2 = mnc->getCpgFrequency(2);
	omega3 = mnc->getCpgFrequency(3);

	//2) add inspectableVaule
	addInspectableValue("R0_outputH0", &R0_H0, "R0_outputH0");
	addInspectableValue("R0_outputH1", &R0_H1, "R0_outputH1");
	addInspectableValue("R0_Pertubation", &R0_Pertubation, "R0_Pertubation");

	addInspectableValue("R1_outputH0", &R1_H0, "R1_outputH0");
	addInspectableValue("R1_outputH1", &R1_H1, "R1_ooutputH1");
	addInspectableValue("R1_Pertubation", &R1_Pertubation, "R1_Pertubation");

	addInspectableValue("L0_outputH0", &L0_H0, "L0_outputH0");
	addInspectableValue("L0_outputH1", &L0_H1, "L0_L0_outputH1");
	addInspectableValue("L0_Pertubation", &L0_Pertubation, "L0_Pertubation");

	addInspectableValue("L1_outputH0", &L1_H0, "L1_outputH0");
	addInspectableValue("L1_outputH1", &L1_H1, "L1_outputH1");
	addInspectableValue("L1_Pertubation", &L1_Pertubation, "L1_Pertubation");

	addInspectableValue("frequency_R0", &omega0, "frequency_R0");
	addInspectableValue("frequency_R1", &omega1, "frequency_R1");
	addInspectableValue("frequency_L0", &omega2, "frequency_L0");
	addInspectableValue("frequency_L1", &omega3, "frequency_L1");
	// add inspectable

	// add inspect some CPG parameter
	//addParameter("stGammaFR", &conf.stGammaFR);
	addInspectableValue("stGammaFR", &conf.stGammaFR, "stGammaFR");

	//addParameter("stGammaRR", &conf.stGammaRR);
	addInspectableValue("stGammaRR", &conf.stGammaRR, "stGammaRR");

	//addParameter("stGammaFL", &conf.stGammaFL);
	addInspectableValue("stGammaFL", &conf.stGammaFL, "stGammaFL");

	//addParameter("stGammaRL", &conf.stGammaRL);
	addInspectableValue("stGammaRL", &conf.stGammaRL, "stGammaRL");
	//gejieduan
	addInspectableValue("stFR_PCPG_H0", &conf.stFR_PCPG_H0, "stFR_PCPG_H0");
	addInspectableValue("stFR_PCPG_H1", &conf.stFR_PCPG_H1, "stFR_PCPG_H1");

	addInspectableValue("stFR_PSN10", &conf.stFR_PSN_10, "stFR_PSN10");
	addInspectableValue("stFR_PSN11", &conf.stFR_PSN_11, "stFR_PSN11");

	addInspectableValue("stFR_VRN", &conf.stFR_VRN, "stFR_VRN");

	addInspectableValue("stFR_PMN1", &conf.stFR_PMN_1, "stFR_PMN1");
	addInspectableValue("stFR_PMN2", &conf.stFR_PMN_2, "stFR_PMN2");

	// Forward model
	/*addInspectableValue("stFMOutput", &conf.stFMOutput, "FMoutput");


	 addInspectableValue("stFMLowpassError", &conf.stFMLowpassError,
	 "stFMLowpassError");*/
	addInspectableValue("stFMcounter", &conf.stFMcounter, "FMcounter");
	addInspectableValue("stFMw20", &conf.stFMw20, "FMw20");
	addInspectableValue("stFMLearnError", &conf.stFMLearnError,
			"stFMLearnError");
	addInspectableValue("stFMOutputfinal", &conf.stFMOutputfinal,
			"stFMOutputfinal");

	// double learn
	addInspectableValue("stDLSInput", &conf.stDLSInput, "stDLSInput");
	addInspectableValue("stDLMInput", &conf.stDLMInput, "stDLMInput");
	addInspectableValue("stDLOutput", &conf.stDLOutput, "stDLOutput");
	addInspectableValue("stDLF", &conf.stDLF, "stDLF");
	addInspectableValue("stDLError", &conf.stDLError, "stDLError");
	addInspectableValue("stDLBias", &conf.stDLBias, "stDLBias");

	// Display gait diagram
	addInspectableValue("stGaitFR", &conf.stGaitDiagramFR, "stGaitFR");
	addInspectableValue("stGaitRR", &conf.stGaitDiagramRR, "stGaitRR");
	addInspectableValue("stGaitFL", &conf.stGaitDiagramFL, "stGaitFL");
	addInspectableValue("stGaitRL", &conf.stGaitDiagramRL, "stGaitRL");
	// debug attitude control parameters
	addInspectableValue("stAttiPmnBiasFR", &conf.stAttiPmnBiasFR,
			"stAttiPmnBiasFR");
	addInspectableValue("stAttiPmnBiasRR", &conf.stAttiPmnBiasRR,
			"stAttiPmnBiasRR");
	addInspectableValue("stAttiPmnBiasFL", &conf.stAttiPmnBiasFL,
			"stAttiPmnBiasFL");
	addInspectableValue("stAttiPmnBiasRL", &conf.stAttiPmnBiasRL,
			"stAttiPmnBiasRL");
	//Orientation
	addInspectableValue("stOriRoll", &conf.stOriRoll, "stOriRoll");
	addInspectableValue("stOriPitch", &conf.stOriPitch, "stOriPitch");
	//GRForce
	addInspectableValue("stGrfFR", &conf.stGrfFR, "stGrfFR");
	addInspectableValue("stGrfRR", &conf.stGrfRR, "stGrfRR");
	addInspectableValue("stGrfFL", &conf.stGrfFL, "stGrfFL");
	addInspectableValue("stGrfRL", &conf.stGrfRL, "stGrfRL");
	// 3) set the parameter
	addParameter("stMI", &conf.stMI);
	addParameter("stPsnSwitchFR", &conf.stPsnSwitchFR);
	addParameter("stPsnSwitchRR", &conf.stPsnSwitchRR);
	addParameter("stPsnSwitchFL", &conf.stPsnSwitchFL);
	addParameter("stPsnSwitchRL", &conf.stPsnSwitchRL);

	addParameter("stVrnSwitchFR", &conf.stVrnSwitchFR);
	addParameter("stVrnSwitchRR", &conf.stVrnSwitchRR);
	addParameter("stVrnSwitchFL", &conf.stVrnSwitchFL);
	addParameter("stVrnSwitchRL", &conf.stVrnSwitchRL);

	addParameter("stJ1SwitchFR", &conf.stJ1SwitchFR);
	addParameter("stJ1SwitchRR", &conf.stJ1SwitchRR);
	addParameter("stJ1SwitchFL", &conf.stJ1SwitchFL);
	addParameter("stJ1SwitchRL", &conf.stJ1SwitchRL);
	if (debug_mode == true) {
		addParameter("stxFRJ1", &conf.stFRJ1);
		addParameter("stxFRJ2", &conf.stFRJ2);
		addParameter("stxFRJ3", &conf.stFRJ3);
		addParameter("styRRJ1", &conf.stRRJ1);
		addParameter("styRRJ2", &conf.stRRJ2);
		addParameter("styRRJ3", &conf.stRRJ3);
		addParameter("stzFLJ1", &conf.stFLJ1);
		addParameter("stzFLJ2", &conf.stFLJ2);
		addParameter("stzFLJ3", &conf.stFLJ3);
		addParameter("stwRLJ1", &conf.stRLJ1);
		addParameter("stwRLJ2", &conf.stRLJ2);
		addParameter("stwRLJ3", &conf.stRLJ3);
	}

}

void modularNeuroController::initialize(int aAMOSversion, bool mCPGs,
		bool mMuscleModelisEnabled) {

	t = 0;
	//the second variable corresponds to the number of cpgs to create,i.e. fourlegs=4
	mnc = new ModularNeural(LEG_NUM);

}

void modularNeuroController::init(int sensornumber, int motornumber,
		RandGen* randGen) {
	numbersensors = sensornumber;
	numbermotors = motornumber;
	x.resize(sensornumber);
	y.resize(motornumber);

}
//utility to draw outputs of the neurons
void modularNeuroController::updateGui() {

	R0_H0 = mnc->getCpgOut0(0);
	R0_H1 = mnc->getCpgOut1(0);
	R0_Pertubation = mnc->getCpgOut2(0);

	R1_H0 = mnc->getCpgOut0(1);
	R1_H1 = mnc->getCpgOut1(1);
	R1_Pertubation = mnc->getCpgOut2(1);

	L0_H0 = mnc->getCpgOut0(2);
	L0_H1 = mnc->getCpgOut1(2);
	L0_Pertubation = mnc->getCpgOut2(2);

	L1_H0 = mnc->getCpgOut0(3);
	L1_H1 = mnc->getCpgOut1(3);
	L1_Pertubation = mnc->getCpgOut2(3);

	omega0 = mnc->getCpgFrequency(0);
	omega1 = mnc->getCpgFrequency(1);
	omega2 = mnc->getCpgFrequency(2);
	omega3 = mnc->getCpgFrequency(3);
//
	conf.stGammaFR = mnc->getDLOutput(0);
	conf.stGammaRR = mnc->getDLOutput(1);
	conf.stGammaFL = mnc->getDLOutput(2);
	conf.stGammaRL = mnc->getDLOutput(3);
	/*
	 conf.stFMOutput = mnc->getFMOutput(0);
	 conf.stFMInput = mnc->getFMInput(0);
	 conf.stFMLowpassError = mnc->getFMLowpassError(0);

	 conf.stFMw20=mnc->getFMw20(0);
	 conf.stFMcounter=mnc->getFMcounter(0);
	 conf.stFMLearnError = mnc->getFMLearnError(0);
	 conf.stFMOutputfinal = mnc->getFMOutputfinal(0);
	 */

	conf.stDLSInput = mnc->getDLSInput(0);
	conf.stDLMInput = mnc->getDLMInput(0);
	conf.stDLOutput = mnc->getDLOutput(0);
	conf.stDLF = mnc->getDLF(0);
	conf.stDLError = mnc->getDLError(0);
	conf.stDLBias = mnc->getDLBias(0);

	//
	conf.stFR_PCPG_H0 = mnc->getPcpgOutput(0, 0);
	conf.stFR_PCPG_H1 = mnc->getPcpgOutput(0, 1);

	conf.stFR_PSN_10 = mnc->getPsnOutput(0, 10);
	conf.stFR_PSN_11 = mnc->getPsnOutput(0, 11);

	conf.stFR_VRN = mnc->getVrnOut(0);

	conf.stFR_PMN_1 = mnc->getPmnOut(0, 1);
	conf.stFR_PMN_2 = mnc->getPmnOut(0, 2);

	conf.stGaitDiagramFR = getGaitDiagram(GRForce.at(0));
	conf.stGaitDiagramRR = getGaitDiagram(GRForce.at(1));
	conf.stGaitDiagramFL = getGaitDiagram(GRForce.at(2));
	conf.stGaitDiagramRL = getGaitDiagram(GRForce.at(3));

	conf.stAttiPmnBiasFR = mnc->getAttitudePmnBias(0);
	conf.stAttiPmnBiasRR = mnc->getAttitudePmnBias(1);
	conf.stAttiPmnBiasFL = mnc->getAttitudePmnBias(2);
	conf.stAttiPmnBiasRL = mnc->getAttitudePmnBias(3);

	conf.stGrfFR = GRForce.at(0);
	conf.stGrfRR = GRForce.at(1);
	conf.stGrfFL = GRForce.at(2);
	conf.stGrfRL = GRForce.at(3);

	conf.stOriRoll = Orientation.at(0);
	conf.stOriPitch = Orientation.at(1);

}

//implement controller here
void modularNeuroController::step(const sensor* x_, int number_sensors,
		motor* y_, int number_motors) {

	assert(number_sensors == numbersensors);
	assert(number_motors == numbermotors);

	//0) Sensor inputs/scaling  ----------------
	for (unsigned int i = 0; i < LILDOG_SENSOR_MAX; i++) {
		x.at(i) = x_[i];
	}
	//1) upper and out layer input ,update inputNeuron Input,1-侧摆关节抑制转移，2-Psn，3-VRN,4-待用
	mnc->setInputNeuronInput(0, conf.stJ1SwitchFR, conf.stPsnSwitchFR,
			conf.stVrnSwitchFR, 0.0);//inputNeuron1 unuse, can be connect to motor neuron,inputNeuron2-ps,3-vrn,4-unuse
	mnc->setInputNeuronInput(1, conf.stJ1SwitchRR, conf.stPsnSwitchRR,
			conf.stVrnSwitchRR, 0.0);
	mnc->setInputNeuronInput(2, conf.stJ1SwitchFL, conf.stPsnSwitchFL,
			conf.stVrnSwitchFL, 0.0);
	mnc->setInputNeuronInput(3, conf.stJ1SwitchRL, conf.stPsnSwitchRL,
			conf.stVrnSwitchRL, 0.0);
	//2) update simulation for body of attitude control
	for (unsigned int i = 0; i < filterOri.size(); i++)
		Orientation.at(i) = filterOri.at(i)->update(x.at(BX_ori + i));
	mnc->setAttituteInput(Orientation.at(1), Orientation.at(0));//update the attitude input
	mnc->updateAttitudeControl();
	//3) update simulation of Legs
	for (unsigned int i = 0; i < LEG_NUM; i++) {
		mnc->setCpgMi(i, conf.stMI);
		GRForce.at(i) = filterGRF.at(i)->update(x.at(R0_fs + i));//filtering of the feedback signal,force signal
		mnc->setGRFInput(i, GRForce.at(i));	//foot contact force ,effect the activity
		mnc->setJAngleInput(i,0,x.at(3*i+0));
		mnc->setJAngleInput(i,1,x.at(3*i+1));
		mnc->setJAngleInput(i,2,x.at(3*i+2));
		mnc->update(i);	//update((feedback0 - 0.4) / 1.5, 0);
		mnc->j1_output.at(i) = 0.05;// there are two effects, one is to keep stable ,another is to prevent rear leg dragging
	}
	//4) control the joints
	if (debug_mode == false) {
		y_[0] = mnc->j1_output.at(0);	//mnc->getVrnOut(0);
		y_[1] = mnc->j2_output.at(0);//1 * mnc->getPsnOutput(0, 10);//(+0.4+mnc->getCpgOut1(0))*1.5;
		y_[2] = mnc->j3_output.at(0);//1.1 * mnc->getPsnOutput(0, 11) + 0.6;//0.4+mnc->getPsnOutput(0,11);

		//rear right
		y_[3] = mnc->j1_output.at(1);//mnc->getVrnOut(1);//0.5;//mnc->getCpgOut1(1);
		y_[4] = mnc->j2_output.at(1);//1 * mnc->getPsnOutput(1, 10);//-0.4+mnc->getCpgOut1(1))*1.5;
		y_[5] = mnc->j3_output.at(1);//1.1 * mnc->getPsnOutput(1, 11) - 0.6;//-0.4-mnc->getPsnOutput(1,11);

		//top left
		y_[6] = mnc->j1_output.at(2);//mnc->getVrnOut(2);//0.5;//mnc->getCpgOut1(2);
		y_[7] = mnc->j2_output.at(2);//1 * mnc->getPsnOutput(2, 10);//(+0.4+mnc->getCpgOut1(2))*1.5;
		y_[8] = mnc->j3_output.at(2);//1.1 * mnc->getPsnOutput(2, 11) + 0.6;//0.4+mnc->getPsnOutput(2,11);

		//rear left
		y_[9] = mnc->j1_output.at(3);//mnc->getVrnOut(3);//mnc->getCpgOut1(3);
		y_[10] = mnc->j2_output.at(3);//1 * mnc->getPsnOutput(3, 10);//(-0.4+mnc->getCpgOut1(3))*1.5;
		y_[11] = mnc->j3_output.at(3);//1 * mnc->getPsnOutput(3, 11) - 0.6;//-0.4-mnc->getPsnOutput(3,11);
	} else {
		y_[0] = conf.stFRJ1;
		y_[1] = conf.stFRJ2;//1 * mnc->getPsnOutput(0, 10);//(+0.4+mnc->getCpgOut1(0))*1.5;
		y_[2] = conf.stFRJ3;//1.1 * mnc->getPsnOutput(0, 11) + 0.6;//0.4+mnc->getPsnOutput(0,11);

		//rear right
		y_[3] = conf.stRRJ1;	//mnc->getVrnOut(1);//0.5;//mnc->getCpgOut1(1);
		y_[4] = conf.stRRJ2;//1 * mnc->getPsnOutput(1, 10);//-0.4+mnc->getCpgOut1(1))*1.5;
		y_[5] = conf.stRRJ3;//1.1 * mnc->getPsnOutput(1, 11) - 0.6;//-0.4-mnc->getPsnOutput(1,11);

		//top left
		y_[6] = conf.stFLJ1;	//mnc->getVrnOut(2);//0.5;//mnc->getCpgOut1(2);
		y_[7] = conf.stFLJ2;//1 * mnc->getPsnOutput(2, 10);//(+0.4+mnc->getCpgOut1(2))*1.5;
		y_[8] = conf.stFLJ3;//1.1 * mnc->getPsnOutput(2, 11) + 0.6;//0.4+mnc->getPsnOutput(2,11);

		//rear left
		y_[9] = conf.stRLJ1;	//mnc->getVrnOut(3);//mnc->getCpgOut1(3);
		y_[10] = conf.stRLJ2;//1 * mnc->getPsnOutput(3, 10);//(-0.4+mnc->getCpgOut1(3))*1.5;
		y_[11] = conf.stRLJ3;//1 * mnc->getPsnOutput(3, 11) - 0.6;//-0.4-mnc->getPsnOutput(3,11);
	}
	//5) update the inspectvalue
	updateGui();
	//6) update the time
	t++;
}

/** stores the controller values to a given file. */
bool modularNeuroController::store(FILE* f) const {
	return true;
}

/** loads the controller values from a given file. */
bool modularNeuroController::restore(FILE* f) {
	//  Configurable::parse(f);
	return true;
}

/**Calculate the gait diagram ,form contact force of foot ***/
double modularNeuroController::getGaitDiagram(double GRF) {
	double contactValue = 0.2;	//接触阀
	if (GRF > contactValue)
		return 1;
	else
		return 0;

}

}
