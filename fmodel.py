#!/usr/bin/env python
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import string
gamma=string.atof(sys.argv[1])
pi=math.pi;
x=np.linspace(0,20*pi,300);
y=np.sin(x);
y=0.6*y;
plt.plot(y);

motor_input_old=y[1];
F_old=0;
F=np.zeros(y.shape)

for i ,j in enumerate(y):
	motor_input=j;
	if motor_input_old > motor_input:
		G=1
	else:
		G=0
	F[i]=gamma*G+(1-gamma)*F_old;
	F_old=F[i];
	motor_input_old=motor_input;
plt.plot(F)
plt.show()

